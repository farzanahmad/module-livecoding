<?php

namespace Market\LiveCoding\Unit;

use Tests\TestCase;

class ProcessorTest extends TestCase
{
    /**
     * @test
     */
    public function testloadBySource()
    {
        $processor = new \Market\LiveCoding\FileProcessor;

        $validFiles = [
            'http://www.mocky.io/v2/5d14e9382f00007ca3c4f51e',
            'http://www.mocky.io/v2/5c6abed9330000cc2e7f4ceb',
            'http://www.mocky.io/v2/5d14e9c72f0000c9c0c4f523',
        ];

        $invalidFiles = [
            'http://www.mocky.io/v2/5d14fc662f00005200c4f558',
            'https://www.mocky.io/assets/img/octocat.png',
        ];

        foreach ($validFiles as $validFile) {
            $this->assertInstanceOf(\Market\LiveCoding\FileProcessor::class, $processor->loadBySource($validFile));
        }

        foreach ($invalidFiles as $invalidFile) {
            $this->expectException(\Exception::class);

            $processor->loadBySource($invalidFile);
        }

    }
}
