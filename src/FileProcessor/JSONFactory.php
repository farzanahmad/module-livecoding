<?php

namespace Market\LiveCoding\FileProcessor;

use Market\LiveCoding\Parser\JSONParser;
use Market\LiveCoding\Parser\ParserInterface;
use Market\LiveCoding\Reader\JSONReader;
use Market\LiveCoding\Reader\ReaderInterface;

class JSONFactory extends AbstractFactory
{

    public function createParser(): ParserInterface
    {
        return new JSONParser();
    }

    public function createReader(): ReaderInterface
    {
        return new JSONReader();
    }

    public static function isValidFile($source): bool
    {
        $data = file_get_contents($source);
        json_decode($data);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}