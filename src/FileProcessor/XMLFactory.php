<?php

namespace Market\LiveCoding\FileProcessor;

use Market\LiveCoding\Parser\ParserInterface;
use Market\LiveCoding\Parser\XMLParser;
use Market\LiveCoding\Reader\ReaderInterface;
use Market\LiveCoding\Reader\XMLReader;

class XMLFactory extends AbstractFactory
{

    public function createParser(): ParserInterface
    {
        return new XMLParser();
    }

    public function createReader(): ReaderInterface
    {
        return new XMLReader();
    }

    public static function isValidFile($source): bool
    {
        $data = file_get_contents($source);
        libxml_use_internal_errors(true);

        $doc = simplexml_load_string($data);

        if (!$doc) {
            return false;
            libxml_clear_errors();
        }
        return true;
    }

}