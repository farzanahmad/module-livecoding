<?php

namespace Market\LiveCoding\FileProcessor;

use Market\LiveCoding\Parser\ParserInterface;
use Market\LiveCoding\Reader\ReaderInterface;

abstract class AbstractFactory
{

    public abstract static function isValidFile($source): bool;

    public abstract function createReader(): ReaderInterface;

    public abstract function createParser(): ParserInterface;

}