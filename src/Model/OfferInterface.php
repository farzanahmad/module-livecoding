<?php

namespace Market\LiveCoding\Model;

/**
 * Interface the Data Transfer Object, that is representation of external JSON Data
 */
interface OfferInterface
{
    const ID = 'id';
    const PRICE = 'price';
    const QUANTITY = 'quantity';

    public function getId();

    public function setId($id);

    public function getPrice();

    public function setPrice($price);

    public function getQuantity();

    public function setQuantity($quantity);

    public function getData($key);

    public function __toArray(): array;
}