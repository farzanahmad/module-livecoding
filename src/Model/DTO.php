<?php

namespace Market\LiveCoding\Model;

abstract class DTO
{
    protected $_data;

    protected function _getData($key)
    {
        if (isset($this->_data[$key])) {
            return $this->_data[$key];
        }
        return null;
    }

    protected function _setData($key, $value)
    {
        $this->_data[$key] = $value;
    }

    public function getData($key)
    {
        return $this->_getData($key);
    }

    public function __toArray(): array
    {
        $data = $this->_data;
        foreach ($data as $key => $value) {
            $data[$key] = $value;
        }
        return $data;
    }
}