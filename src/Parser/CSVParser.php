<?php

namespace Market\LiveCoding\Parser;

use Market\LiveCoding\Model\OfferCollectionInterface;

class CSVParser extends AbstractParser implements ParserInterface
{
    /**
     * @param string $input
     * @return OfferCollectionInterface
     */
    public function parse(string $input): OfferCollectionInterface
    {
        $lines = explode(PHP_EOL, $input);
        $data = [];
        $offers = [];
        if (count($lines) > 0) {
            $header = str_getcsv($lines[0]);
            $lineCount = 0;
            foreach ($lines as $line) {
                if ($lineCount++ < 1) {
                    continue;
                }
                if (count(str_getcsv($line)) == count($header)) {
                    $data[] = array_combine($header, str_getcsv($line));
                }
            }
        }

        /** @var array $record */
        foreach ($data as $record) {
            $offer = $this->offerFactory->create();
            $offer->setId(isset($record['id']) ? $record['id'] : null);
            $offer->setPrice(isset($record['price']) ? $record['price'] : 0);
            $offer->setQuantity(isset($record['quantity']) ? $record['quantity'] : 0);

            $this->offerCollection->add($offer);
        }

        return $this->offerCollection;
    }
}